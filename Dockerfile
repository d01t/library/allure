FROM openjdk:11

ARG VERSION=2.7.0

COPY install.sh /root/install.sh

RUN /bin/bash /root/install.sh ${VERSION}
