#!/bin/bash
VERSION=$1
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
apt-get --yes --quiet=2 update
apt-get --yes --quiet=2 install \
	ca-certificates \
	curl \
	jq \
	libxml2-utils \
	rsync \
	tar \
	uuid \
	unzip \
	xsltproc \
	zip
curl -sSL -o allure.tgz https://dl.bintray.com/qameta/generic/io/qameta/allure/allure/${VERSION}/allure-${VERSION}.tgz
mkdir -p /opt/allure
tar --strip-components=1 -C /opt/allure/ -xvf allure.tgz
ln -s /opt/allure/bin/allure /usr/local/bin/
rm allure.tgz
apt-get clean
rm -rf /var/lib/apt/lists/*
